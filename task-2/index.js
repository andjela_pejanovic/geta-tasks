const newsApiKey = '8363f45619fc47e9af1ea4034ae457b0';

const fromDateToString = date => `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
const subtractDays = (date, days) => new Date(date.setDate(date.getDate() - days));

const getRenderedArticle = article => `
    <div class="card">
          <a class="card-head" href="${article.url}" target="_blank">${article.title}</a>
          <img class="card-image" src="${article.urlToImage}">
          <div class="card-body"> ${article.description} </div>             
    </div>
`;

const renderArticles = articles => {
    document.getElementById('articles').innerHTML = articles.reduce((acc, article) => acc + getRenderedArticle(article), '');
};

const getNewsUrl = (dateFrom, dateTo) => `https://newsapi.org/v2/everything?q=us&sortBy=popularity&from=${dateFrom}&to=${dateTo}&apiKey=${newsApiKey}`;

const fetchNews = (dateFrom, dateTo) => {
    const newsUrl = getNewsUrl(fromDateToString(dateFrom), fromDateToString(dateTo));

    fetch(newsUrl)
        .then(response => response.json())
        .then(({ articles }) => renderArticles(articles))
        .catch(console.error);
};

const selectMenuItem = (menuItemId) => {
    document.querySelectorAll('.menu-item').forEach(el => {
        if (el.id === menuItemId) {
            el.classList.add('selected-menu-item');
        } else {
            el.classList.remove('selected-menu-item');
        }
    });
};

document.querySelectorAll('.menu-item').forEach(menuItem => menuItem.addEventListener('click', ({ target: { id } }) => selectMenuItem(id)));

document.getElementById('today').addEventListener('click', () => fetchNews(new Date(), new Date()));
document.getElementById('yesterday').addEventListener('click', () => fetchNews(subtractDays(new Date(), 1), subtractDays(new Date(), 1)));
document.getElementById('last-week').addEventListener('click', () => fetchNews(subtractDays(new Date(), 7), new Date()));

fetchNews(new Date(), new Date());
