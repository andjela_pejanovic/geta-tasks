### AppicationForm.aspx

- The code should be better formatted. Lots of lines are too long and it is hard to read
- Lines 4-12: Registration of common templates should be moved into `web.config` in order to avoid duplication of the same code into multiple pages (reusability)
- Line 13: ID should be meaningful, `Content1` is not good solution
- Naming convention for fields is not uniform:
    - Line 25: County DDL ID: `Ddl_County`
    - Line 67: Show File Uppload Button ID: `btnShowFileUpload`
    - Line 69: File Upload Panel ID: `pnlFileUpload`
    - Line 85: Submit Form Button ID: `Btn_SubmitForm`
- Regexes for validation should be centralized so that they can be used from multiple places
- Some of the fields’ properties should be moved from template (as in MVC is often in View Model)
- JS and CSS code should be extracted into separated files
- Line 197: `text` variable is not used, so it could be deleted or used in expression below:
`$(this).parent().siblings('.filename').text(text);`
- Line 206: `return` statement inside of `$.each` callback does not make sense

### ApplicationForm.aspx.cs

- Line 17: `countyList` is probably a list of entities that is used in multiple places and should be centralized (in database, enum). Also, it should be initialized in other way, if possible by using DI
- `ContactPerson` class - domain classes should be extracted from business logic, at least in another file or often in a separate project (DAL)
- Email handling region should be moved to the separate service (single responsibility principle)
- `BuildEmailContent` method - could be written using string interpolation:
`$"<table> {ContentStart} … </table>"`
- Line 68: `if (contactPerson.Municipality == "mrHeroy")` - Extract the hardcoded values into constants, possibly into a separate file
- Line 96-98: The code can be written more elegantly: `Enumerable.Range(0, numberOfFiles).ToList();`
- Line 257: foreach should have a break statement because there is no need to iterate throughout the list
- Lines 251-258: could be written as
`ok = mail.To.All(x => StringValidationUtil.IsValidEmailAddress(x.singleToAddress.Address));`
